// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr: true,
  buildModules: [
    '@nuxtjs/style-resources',
    '@nuxt/typescript-build'
  ],
  styleResources: {
    scss: [
      '@/assets/css/variables.scss',
      '@/assets/css/navbar.scss',
      '@/assets/css/main.scss',
    ],
  },
  css: [
      '@/assets/css/variables.scss',	
      '@/assets/css/main.scss',	
  ],
  build: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ],
    extend(config, ctx) {}
  },
  components: [
    { path: '~/src/components/button' },
    { path: '~/src/components/partials' },
    '~/components'
  ]
})
